import * as React from 'react'
import { useState, useEffect } from 'react';
import { useSelector } from "react-redux";
import { NavFrame, OpWalletState, AddressData, getActiveWallet, getActiveAddress, AddressChecker } from 'op-wallet';
import { SpacerRow } from '../SpacerRow';
import { AddressIdentifiersDropdown } from './components/AddressIdentifiersDropdown';
import { QueryResults, getMemoPostsForAddress } from './bitdb';
import { MemoPost } from './components/MemoPost';
import useLocalStorage from '@rehooks/local-storage';
import { UserPostHistory, MY_POST_TXIDS_LOCAL_STORAGE_KEY } from './storage';


type LoadingStatus = "none" | "loading" | "load-success" | "load-error"


interface MyPostsPageProps { }

type AllProps = MyPostsPageProps


export const MyPostsPage = (props: AllProps) => {

  const [loaded, setLoaded] = useState("none" as LoadingStatus);
  const [posts, setPosts] = useState({u:[], c:[]} as QueryResults);
  const [postHistories, ] = useLocalStorage<UserPostHistory[]>(MY_POST_TXIDS_LOCAL_STORAGE_KEY);

  const activeWallet = useSelector((state: OpWalletState) => getActiveWallet(state));
  const activeAddress = useSelector((state: OpWalletState) => getActiveAddress(state));

  const addresses = activeWallet.walletAddresses;

  const getMyAddress = (): AddressData => {
    if (activeAddress) {
      return activeAddress;
    }

    return addresses[0];
  }

  const myAddress = getMyAddress();
  const myPostHistory = (postHistories || []).find(p => p.address === myAddress.cashAddress);

  const [selectedAddress, setSelectedAddress] = React.useState(myAddress);


  useEffect(() => {
    const getMemoPosts = async () => {
      if (loaded !== "none") {
        return;
      }
  
      try {
        setLoaded("loading")
        const posts = await getMemoPostsForAddress(myAddress.cashAddress, myPostHistory !== undefined ? myPostHistory.postTxIds : undefined);
  
        if (posts) {
          setPosts(posts);
          setLoaded("load-success");
        } else {
          setLoaded("load-error");
        }
      } catch (e) {
        setLoaded("load-error");
      }
    }

    getMemoPosts();
  }, [loaded, myAddress, myPostHistory]);

  
  return (
    <NavFrame>
      <AddressChecker>
        <SpacerRow/>
        <h4>My Memo Posts</h4>
        <SpacerRow/>

        <AddressIdentifiersDropdown
          onAddressSelected={(address: AddressData) => {
            setSelectedAddress(address);
            setLoaded("none");
          }}
          selectedAddress={selectedAddress}
          addresses={addresses}
          />

        {loaded === "load-error" ? 
          <div>Error loading data</div> :
          <>
            {posts.u.map(p => <MemoPost message={p.msg} key={p.txid}></MemoPost>)}
            {posts.c.map(p => <MemoPost message={p.msg} key={p.txid}></MemoPost>)}
          </>
        }
      </AddressChecker>
    </NavFrame>
  )
}
