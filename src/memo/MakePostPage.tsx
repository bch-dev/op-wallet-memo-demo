import * as React from 'react'
import { useState } from 'react';
import { useSelector } from "react-redux";
import { NavFrame, OpWalletState, getActiveAddress } from 'op-wallet';
import { Input, Label, FormGroup, Button, Alert } from 'reactstrap';
import { buildPostMemoOpReturn } from './post';
import { getExplorerUrlForTxid } from '../utils';
import { getBitBoxForEnv } from 'bbnv'
import { addMemoUserPost, MY_POST_TXIDS_LOCAL_STORAGE_KEY, UserPostHistory } from './storage';
import { OpChopTxParameters } from 'bbnv/dist/opchop';
import { sendOpChopMessage } from 'bbnv/dist/opchop/utils';
import useLocalStorage from '@rehooks/local-storage';


type PostStatus = "none" | "posting" | "post-success" | "post-error"


export const MakePostPage = () => {

  const activeAddress = useSelector((state: OpWalletState) => getActiveAddress(state));

  const [postMessage, setPostMessage] = useState("abc12")
  const [postStatus, setPostStatus] = useState("none" as PostStatus)
  const [txid, setTxid] = useState("");

  const [memoPosts, setMemoPosts] = useLocalStorage<UserPostHistory[]>(MY_POST_TXIDS_LOCAL_STORAGE_KEY);

  const makePost = async () => {
    if (!activeAddress) {
      return;
    }

    const opReturn = buildPostMemoOpReturn(postMessage);
    const msg: OpChopTxParameters = {
      paymentKeyWif: activeAddress.paymentKeyWif,
      opReturnData: opReturn
    } as OpChopTxParameters

    try {
      const txid = await sendOpChopMessage(msg);
      setPostStatus("post-success");
      setTxid(txid);

      if (memoPosts !== null) {
        const updatedPosts = await addMemoUserPost(memoPosts, activeAddress.cashAddress, txid);
        setMemoPosts(updatedPosts);
      }

    } catch (e) {
      setPostStatus("post-error");
    }
  }

  const updateMemoMessage = (newMessage: string) => {
    setPostMessage(newMessage);
  }

  const PostPageAlert = () => {
    const BITBOXNetName = getBitBoxForEnv().NetName;

    switch (postStatus) {
      case "none":
        return <Alert color="primary">
          No message sent yet
        </Alert>
      case "posting":
          return <Alert color="warning">
            Sending message
          </Alert>
      case "post-success":
          return <Alert color="success">
            Memo sent - <a href={getExplorerUrlForTxid(txid, BITBOXNetName)} target="_blank" rel="noopener noreferrer">See on blockchain</a>
          </Alert>
      case "post-error":
          return <Alert color="danger">
            Error sending message
          </Alert>
    }
  }

  React.useEffect(() => {
    if (!memoPosts) {
      setMemoPosts([]);
    }
  }, [memoPosts, setMemoPosts]);

  return (
    <NavFrame>
      <FormGroup>
        <Label for="memo-text">Memo Message</Label>
        <Input type="textarea" id="memo-text" 
          rows="10" 
          defaultValue={postMessage}
          onChange={e => updateMemoMessage(e.target.value)}
          />
      </FormGroup>
      {<PostPageAlert />}
      <Button color="primary" block={true} onClick={() => makePost()}>Post to Memo</Button>
    </NavFrame>
  )
}
