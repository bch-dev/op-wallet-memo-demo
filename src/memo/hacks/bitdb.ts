import { getBbEnvForNetworkType } from 'bbnv';
import { TxId, Address } from "bbnv/dist/opchop";
import { getNetworkTypeForAddress } from 'bbnv/dist/opchop/utils'

const BITDB_ADDRESS_VERSIONS_KEY = "BITDB-ADDRESS-MAPPINGS";

interface AddrMap {
  readonly address: string
  readonly bitdbVersion: string
}


const getBitdbAddressForTxId = async (address: Address, txid: TxId): Promise<string> => {

  const networkType = getNetworkTypeForAddress(address);
  const bitbox = getBbEnvForNetworkType(networkType).BBSdk;

  const query = {
    "v": 3,
    "q": {
      "find": { 
        "tx.h": txid
      }
    },
    "r": { 
      "f": "[ .[] | {addr: .in[0].e.a, msg: .out[0].s2, txid: .tx.h} ]"
    }
  }

  const queryResults = await bitbox.BitDB.get(query);
  console.log(queryResults)

  if (queryResults.c.length > 0) {
    return queryResults.c[0].addr;
  }

  return "";
}


export const getBitdbVersionOfAddress = async (address: string, postTxIds: string[] | undefined): Promise<string | undefined> => {
  const bitdbAddrVersionsStr = localStorage.getItem(BITDB_ADDRESS_VERSIONS_KEY);

  if (bitdbAddrVersionsStr) {
    const bitdbAddrVersions = JSON.parse(bitdbAddrVersionsStr) as AddrMap[]
    const found = bitdbAddrVersions.find(m => m.address === address);
    if (found) {
      return found.bitdbVersion;
    }
  }

  if (postTxIds && postTxIds.length > 0) {
    const bitdbAddr = await getBitdbAddressForTxId(address, postTxIds[0]);
    return bitdbAddr;
  }

  return undefined;
}


export const saveBitdbVersionOfAddress = (address: string, bitdbVersion: string): void => {
  var bitdbAddrVersionsStr = localStorage.getItem(BITDB_ADDRESS_VERSIONS_KEY);

  let bitdbAddrVersions: AddrMap[] = [];
  if (bitdbAddrVersionsStr) {
    bitdbAddrVersions = JSON.parse(bitdbAddrVersionsStr) as AddrMap[]
  }

  const found = bitdbAddrVersions.find(m => m.address === address);
  if (!found) {
    const mapping: AddrMap = {
      address: address,
      bitdbVersion: bitdbVersion
    }
    bitdbAddrVersions.push(mapping);

    const updatedStr = JSON.stringify(bitdbAddrVersions);
    localStorage.setItem(BITDB_ADDRESS_VERSIONS_KEY, updatedStr);
  }
}
