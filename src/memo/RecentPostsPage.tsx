import * as React from 'react'
import { useState, useEffect } from 'react';
import { useSelector } from "react-redux";
import { NavFrame, OpWalletState, getActiveWallet } from 'op-wallet';
import { getBbEnvForNetworkType } from 'bbnv';
 

type LoadingStatus = "none" | "loading" | "load-success" | "load-error"

interface QueryResult {
  readonly addr: string
  readonly msg: string
  readonly txid: string
}

interface QueryResults {
  readonly c: QueryResult[]
  readonly u: QueryResult[]
}

const isQueryResults = (value: any): value is QueryResults => {
  return (value as QueryResults).c !== undefined && (value as QueryResults).u !== undefined;;
}


interface MemoPostProps {
  readonly message: string
}

const MemoPost = (props: MemoPostProps) => {
  return <div>{props.message}</div>
}


interface RecentPostsPageProps { }


type AllProps = RecentPostsPageProps


export const RecentPostsPage = (props: AllProps) => {

  const [loaded, setLoaded] = useState("none" as LoadingStatus);
  const [posts, setPosts] = useState({u:[], c:[]} as QueryResults);

  const activeWallet = useSelector((state: OpWalletState) => getActiveWallet(state));


  useEffect(() => {
    if (loaded !== "none") {
      return;
    }

    const bbnv = getBbEnvForNetworkType(activeWallet.walletNetworkType);
    const bitbox = bbnv.BBSdk;

    setLoaded("loading")
    bitbox.BitDB.get(
      {
        "v": 3,
        "q": {
          "find": { "out.b0": { "op": 106 }, "out.h1": "6d02" },
          "limit": 50
        },
        "r": {
          "f": "[ .[] | {addr: .in[0].e.a, msg: .out[0].s2, txid: .tx.h} ]"
        }
      }
    ).then((value: any) => {
      if (isQueryResults(value)) {
        console.log(value)
        setPosts(value)
        setLoaded("load-success")
      } else {
        setLoaded("load-error");
      }
    }).catch((err: Error) => {
      setLoaded("load-error");
    })
  }, [loaded, activeWallet.walletNetworkType]);

  return <NavFrame>
    {loaded === "load-error" ? 
      <div>Error loading data</div> :
      <>
        {posts.u.map(p => <MemoPost message={p.msg} key={p.txid}></MemoPost>)}
        {posts.c.map(p => <MemoPost message={p.msg} key={p.txid}></MemoPost>)}
      </>
    }
  </NavFrame>
}
