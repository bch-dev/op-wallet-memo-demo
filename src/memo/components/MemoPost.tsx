import React from "react"


export interface MemoPostProps {
  readonly message: string
}

export const MemoPost = (props: MemoPostProps) => {
  return <div>{props.message}</div>
}
