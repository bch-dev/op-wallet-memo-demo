import React from "react"
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap"
import { AddressData } from "op-wallet";


interface Props {
  selectedAddress: AddressData
  addresses: AddressData[]

  onAddressSelected: (address: AddressData) => void
}

type AllProps = Props


export const AddressIdentifiersDropdown = (props: AllProps) => {

  const [dropdownOpen, setDropdownOpen] = React.useState(false);

  const toggle = () => setDropdownOpen(prevState => !prevState);


  return (
    <Dropdown isOpen={dropdownOpen} toggle={toggle}>
      <DropdownToggle block caret>
        {props.selectedAddress.label ? props.selectedAddress.label : props.selectedAddress.cashAddress}
      </DropdownToggle>
      <DropdownMenu>
        {props.addresses.map(a => (
          <DropdownItem 
            key={a.cashAddress}
            onClick={() => props.onAddressSelected(a)}
            >
            {a.label ? a.label : a.cashAddress}
          </DropdownItem>)
        )}
      </DropdownMenu>
    </Dropdown>
  )
}
