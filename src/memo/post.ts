import { getBitBoxForEnv } from 'bbnv';


export interface PostMemoMessage {
  readonly privateKey: string
  readonly messageBody: string
}


export const buildPostMemoOpReturn = (message: string): Buffer => {
  const BITBOX = getBitBoxForEnv().BBSdk;
  // const BITBOXNetName = getBitBoxForEnv().NetName;

  const messageBuffer = Buffer.from(message, 'utf8');
  if (messageBuffer.byteLength > 217) {
    throw new Error("Message can't be more than 217 bytes. Content: " + message);
  }

  const opReturn = BITBOX.Script.encode([
      BITBOX.Script.opcodes.OP_RETURN,
      // PUSH_2_BYTES,
      Buffer.from("6d02", 'hex'),
      BITBOX.Script.opcodes.OP_PUSHDATA1,
      // messageBuffer.byteLength,
      messageBuffer
    ]);

  return opReturn;
}
