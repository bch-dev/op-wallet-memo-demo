import { Address } from "bbnv/dist/opchop";
import { getBbEnvForNetworkType } from "bbnv";
import { getBitdbVersionOfAddress } from './hacks/bitdb';


interface QueryResult {
  readonly addr: string
  readonly msg: string
  readonly txid: string
}

export interface QueryResults {
  readonly c: QueryResult[]
  readonly u: QueryResult[]
}


export const getMemoPostsForBitdbAddress = async (bitdbAddress: Address): Promise<QueryResults> => {
  if (bitdbAddress === "") {
    return {
      u: [],
      c: []
    }
  }

  const bbEnv = getBbEnvForNetworkType("testnet");
  const bitbox = bbEnv.BBSdk;

  const queryResults = await bitbox.BitDB.get(
    {
      "v": 3,
      "q": {
        "find": {
          "in.e.a": bitdbAddress,
          "out.b0": { "op": 106 }, "out.h1": "6d02" 
        },
        "limit": 50
      },
      "r": { 
        "f": "[ .[] | {addr: .in[0].e.a, msg: .out[0].s2, txid: .tx.h} ]"
      }
    }
  );

  if (queryResults.errors) {
    throw Error(queryResults.errors)
  }

  return queryResults;
}


export const getMemoPostsForAddress = async (address: Address, postTxIds: string[] | undefined): Promise<QueryResults> => {
  
  const bitdbAddrVersion = await getBitdbVersionOfAddress(address, postTxIds);

  if (!bitdbAddrVersion) {
    return {
      u: [],
      c: []
    }
  }

  return await getMemoPostsForBitdbAddress(bitdbAddrVersion);
}
