// import { getBitdbAddressForTxid } from "./bitdb";
// import { getBbEnvForNetworkType } from "bbnv";
import { Address, TxId } from "bbnv/dist/opchop";

export const MY_POST_TXIDS_LOCAL_STORAGE_KEY = "My-Memo-Posts";


export interface UserPostHistory {
  address: Address
  // bitdbAddress: Address
  // networkType: BchNetworkTypes
  postTxIds: TxId[]
}


export const addMemoUserPost = async (posts: UserPostHistory[], address: Address, txid: TxId): Promise<UserPostHistory[]> => {
  const indexOfInterest = posts.findIndex(x => x.address === address);

  if (indexOfInterest === -1) {
    // const bbEnv = getBbEnvForNetworkType("mainnet");
    // const addBB = bbEnv.BBSdk;
  
    // const networkType = addBB.Address.detectAddressNetwork(address);
  
    const newPostData: UserPostHistory = {
      address: address,
      postTxIds: [txid],
      // bitdbAddress: "",
      // networkType: networkType
    }
    
    posts.push(newPostData);
    return posts;
  }

  const postsOfInterest = posts[indexOfInterest];
  postsOfInterest.postTxIds.push(txid);

  return posts;
}


