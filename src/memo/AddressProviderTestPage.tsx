import * as React from 'react'
import { NavFrame, AddressChecker } from 'op-wallet';


interface AddressProviderTestPageProps {

}


export const AddressProviderTestPage = (props: AddressProviderTestPageProps) => {

  return (
    <NavFrame>
      <AddressChecker>
        <div>Has Address</div>
      </AddressChecker>
    </NavFrame>
  )
}
