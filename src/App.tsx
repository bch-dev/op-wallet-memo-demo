import * as React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { WalletPage, AboutPage, NavItemDetails, configNavTitle, configNavLinks, OpWalletFrame } from 'op-wallet'
import { MakePostPage } from './memo/MakePostPage'
import { RecentPostsPage } from './memo/RecentPostsPage'
import { MyPostsPage } from './memo/MyPostsPage'
import { AddressProviderTestPage } from './memo/AddressProviderTestPage'
import { initBbnv, useTestnetNetwork } from 'bbnv'

const navItemDetails: NavItemDetails[] = [
  {
    linkTo: "/memo",
    title: "Memo"
  },
  {
    linkTo: "/recent",
    title: "Recent Posts"
  },
  {
    linkTo: "/myposts",
    title: "My Posts",
  },
  {
    linkTo: "/crypto",
    title: "Wallet"
  },
  {
    linkTo: "/about",
    title: "About Op-Wallet"
  }
]

configNavTitle("op-wallet-memo-demo");
configNavLinks(navItemDetails);
// configNavAboutPageNetworkSelectionEnabled(false);

initBbnv(useTestnetNetwork);

const App = () => {

  return (
    <OpWalletFrame>
      <Router basename={process.env.PUBLIC_URL}>
        <Switch>
          <Route exact={true} path="/" component={WalletPage}/>
          <Route path="/crypto" component={WalletPage} />
          <Route path="/memo" component={MakePostPage} />
          <Route path="/recent" component={RecentPostsPage} />
          <Route path="/myposts" component={MyPostsPage} />
          <Route path="/about" component={AboutPage} />
          <Route path="/test" component={AddressProviderTestPage} />
        </Switch>
      </Router>
    </OpWalletFrame>
  )
}

export default App;
