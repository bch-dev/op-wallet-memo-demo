import { Row } from "reactstrap"
import React from "react"

interface Props {
  paddingTop?: number
  paddingBottom?: number
}


export const SpacerRow = ({paddingTop=10, paddingBottom=10}: Props) => {
  return <Row style={{paddingTop:paddingTop, paddingBottom:paddingBottom}} />
}
