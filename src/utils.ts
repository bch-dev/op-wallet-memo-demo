import { NetworkName } from "bbnv";


export const getExplorerUrlForAddress = (address: string, netname: NetworkName): string => {
  let explorerPath = "https://explorer.bitcoin.com/bch";

  if (netname === "bchtest") {
    explorerPath = "https://explorer.bitcoin.com/tbch";
  }
  
  return explorerPath + "/address/" + address;
}

export const getExplorerUrlForTxid = (txid: string, netname: NetworkName): string => {
  let explorerPath = "https://explorer.bitcoin.com/bch";

  if (netname === "bchtest") {
    explorerPath = "https://explorer.bitcoin.com/tbch";
  }
  
  return explorerPath + "/tx/" + txid;
}